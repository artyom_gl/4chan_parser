FROM python:3.7-slim-buster
COPY . ./4chan_parse
WORKDIR 4chan_parse
RUN pip3 install --no-cache-dir -r requirements.txt