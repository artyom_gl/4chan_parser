DOCKER_COMPOSE=`which docker-compose`
DOCKER_COMPOSE_FILE=docker-compose.yml

install:
	$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) up -d --build
