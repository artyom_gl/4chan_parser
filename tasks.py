from celery import shared_task
from pymongo import MongoClient
from time import sleep

from main import Board, Config, File


@shared_task(name='parse_board', queue='board')
def parse_board():
    board = Board()
    board.parse_all_pages_threads_previews()
    for thread in board.thread_list:
        success = thread.filter_by_regex()
        if success:
            images = thread.get_filenames()
            parse_thread.delay(images)


@shared_task(name='parse_thread', queue='thread')
def parse_thread(files):
    client = MongoClient('mongo', 27017)
    db = client.database
    collection = db['file_names_and_hashes']
    cooldown = Config().file_cooldown
    for file_name in files:
        File(file_name, collection).process()
        sleep(cooldown)
