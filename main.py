import hashlib
import json
import re
import requests
from bs4 import BeautifulSoup as Soup
from pymongo.collection import Collection
from requests import Response
from time import sleep
from typing import Any, Optional, List


class Config:
    """
    Class to provide access to config.json values
    """

    def __init__(self, file='config.json'):
        with open(file, 'r') as file_:
            self.config = json.load(file_)

    def _get_value_by_key(self, key: str) -> Any:
        try:
            return self.config[key]
        except KeyError:
            raise KeyError('config.json is invalid, follow README.md')

    @property
    def board_name(self) -> str:
        return self._get_value_by_key('board_name')

    @property
    def board_cooldown(self) -> str:
        return self._get_value_by_key('board_cooldown')

    @property
    def allowed_file_extensions(self) -> list:
        return self._get_value_by_key('allowed_file_extensions')

    @property
    def regex(self) -> str:
        return self._get_value_by_key('regex')

    @property
    def file_size_limit(self) -> int:
        return int(self._get_value_by_key('file_size_limit'))

    @property
    def file_cooldown(self) -> int:
        return int(self._get_value_by_key('file_cooldown'))

    @property
    def cdn_url_prefix(self) -> str:
        return self._get_value_by_key('cdn_url_prefix')

    @property
    def boards_url_prefix(self) -> str:
        return self._get_value_by_key('boards_url_prefix')

    @property
    def connection_timeout(self) -> int:
        return int(self._get_value_by_key('connection_timeout'))

    @property
    def connection_retries(self) -> int:
        return int(self._get_value_by_key('connection_retries'))

    @property
    def request_headers(self) -> dict:
        return self._get_value_by_key('request_headers')

    @property
    def pagination_cooldown(self) -> int:
        return int(self._get_value_by_key('pagination_cooldown'))


class Parser(Config):
    """
    Implement basic http response functionality with further conversation into bs4 object
    """
    def make_request(self, url: str, stream: bool = False) -> Optional[Response]:
        for retry in range(self.connection_retries):
            try:
                return requests.get(url, stream=stream, headers=self.request_headers, timeout=self.connection_timeout)
            except Exception as e:
                print(e)

    def get_soup(self, url: str) -> Optional[Soup]:
        response = self.make_request(url)
        return Soup(response.content, 'lxml') if response else None


class Board(Parser):
    """
    Board parser for first 10 pages, collect links to threads
    """
    def __init__(self):
        super().__init__()
        self.thread_list: List[Thread] = []
        self.pagination = self._get_pagination_links()
        self.current_page: Optional[str] = None
        self.html: Optional[Soup] = None

    def parse_all_pages_threads_previews(self):
        while self.pagination:
            if self._step_page():  # always before 'get_page_threads_links'
                self._parse_threads_previews()
            else:
                sleep(self.pagination_cooldown)

    def _next_page_link(self) -> Optional[str]:
        """Pop next link if there any"""
        return self.pagination.pop(0) if self.pagination else None

    def _step_page(self) -> bool:
        """Make request with updating html"""
        if self.pagination:
            self.current_page = self._next_page_link()
            self.html = self.get_soup(self.current_page)
            if self.html:
                return True
        return False

    def _parse_threads_previews(self):
        """Parse thread OP-post"""
        threads_holder = self.html.find('div', class_='board')
        threads = threads_holder.find_all('div', class_='thread')
        for thread in threads:
            id_ = thread.attrs.get('id')[1:]
            title_text = thread.find('blockquote', class_='postMessage').text
            thread_obj = Thread(id_=id_, title_text=title_text)
            self.thread_list.append(thread_obj)

    def _get_pagination_links(self) -> List[str]:
        """assuming that board always have 10 pages"""
        link_list = []
        base_board_link = f'{self.boards_url_prefix}/{self.board_name}'
        link_list.append(base_board_link)
        for page in range(2, 11):
            link_list.append(f'{base_board_link}/{str(page)}/')
        return link_list


class Thread(Parser):
    """
    Check thread header(OP post) by regex, if pattern is found
    parse thread for files and then filter files by allowed extensions
    """
    def __init__(self, id_: str, title_text: str):
        super().__init__()
        self.id_ = id_
        self.title_text = title_text
        self.complete_url: str = f'http://boards.4chan.org/{self.board_name}/thread/{self.id_}/'

    def filter_by_regex(self) -> list:
        """Lookup for certain words in a OP post"""
        return re.findall(self.regex, self.title_text)

    def get_filenames(self) -> Optional[List[str]]:
        """Collect all links to attached files from replies and filter them by allowed file extensions"""
        html = self.get_soup(self.complete_url)
        if html:
            image_links = html.findAll('a', class_='fileThumb')
            if image_links:
                all_extensions = [
                    image.get('href')
                    for image in image_links
                ]
                return [
                    image.split('/')[-1]
                    for image in all_extensions
                    if image.split('.')[-1] in self.allowed_file_extensions
                ]


class File(Parser):
    """
    Performs file processing with db names and hashes persistence, write-safe
    When find a new(unique) file saves it to images/ folder
    """
    def __init__(self, file_name: str, collection: Collection):
        super().__init__()
        self.collection = collection
        self.file_name = file_name
        self.file_hash: str = ''
        self.response: Optional[Response] = None

    def process(self):
        """
        Firstly we ensure that filename not present in the database (before downloading it)
        then we apply file-size filter and after we check absence of file md5-checksum in the database
        if all conditions is satisfied, we write down filename and md5-checksum to database
        to ensure uniqueness of the files in a future
        """
        if not self._is_file_already_known(self.file_name):
            self.response = self.make_request(self.cdn_url_prefix + self.board_name + '/' + self.file_name, stream=True)
            if self.response and self.response.status_code == 200:
                if self._is_size_allowed():
                    self.file_hash = self._get_file_checksum()
                    if not self._is_file_already_known(self.file_hash):
                        self._persist_value_to_db(self.file_hash)
                        self._persist_value_to_db(self.file_name)
                        self._save_file_to_disc()
                else:
                    self._persist_value_to_db(self.file_name)

    def _save_file_to_disc(self):
        with open(f'images/{self.file_name}', 'wb') as file_:
            file_.write(self.response.content)
        print(f'{self.file_name} has been downloaded')

    def _persist_value_to_db(self, value: str):
        self.collection.update_one({'_id': value}, {"$set": {'_id': value}}, **{'upsert': True})

    def _is_file_already_known(self, value: str) -> Optional[dict]:
        return self.collection.find_one({'_id': value})

    def _get_file_size(self) -> int:
        return int(self.response.headers.get('Content-length', self.file_size_limit))

    def _is_size_allowed(self) -> bool:
        return self._get_file_size() < self.file_size_limit

    def _get_file_checksum(self) -> str:
        return hashlib.md5(self.response.content).hexdigest()


if __name__ == '__main__':
    """
    Boilerplate for debug purposes
    needs forwarded ports from mongodb (check in docker-compose) and working mongodb container
    """
    from pymongo import MongoClient
    board = Board()
    board.parse_all_pages_threads_previews()
    for thread in board.thread_list:
        success = thread.filter_by_regex()
        if success:
            images = thread.get_filenames()
            client = MongoClient('0.0.0.0', 27017)
            db = client.database
            collection = db['file_names_and_hashes']
            cooldown = Config().file_cooldown
            for file_name in images:
                File(file_name, collection).process()
                sleep(cooldown)
