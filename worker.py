from celery import Celery
from main import Config

app = Celery(include=('tasks',))

app.conf.beat_schedule = {
    'parse_board': {
        'task': 'parse_board',
        'schedule': Config().board_cooldown,
    }}
